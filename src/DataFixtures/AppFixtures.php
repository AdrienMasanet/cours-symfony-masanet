<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    $faker = Faker\Factory::create('fr-FR'); // Appel de l'objet Faker

    for ($i=0; $i <30; $i++) {
      $article = new Article();
      $article->setTitre($faker->sentence(6, true))
              ->setAuteur($faker->name())
              ->setEditeur($faker->name())
              ->setResume($faker->text(250))
              ->setDatePublication($faker->dateTime("now"));
      $manager->persist($article);
    }

    // $product = new Product();
    // $manager->persist($product);

    $manager->flush();
  }
}
