<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; // On charge la classe "Route" permettant l'utilisation des routes dans les annotations
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Article;
use App\Form\CreateArticleFormType;
use App\Repository\ArticleRepository;

class PageController extends AbstractController
{

  private $articleRepository;

  public function __construct(ArticleRepository $articleRepository)
  {
    // Injection globale sur une propriété du controller pour éviter le chainage inutile
    $this->articleRepository = $articleRepository;
  }

  /**
   * @Route("/", name="accueilPage", methods={"GET", "POST"})
   */
  public function accueilPage(Request $request)
  {
    $allAuthors = [];

    foreach ($this->articleRepository->findAllAuthors() as $authorElement) {
      array_push($allAuthors, $authorElement["auteur"]);
    }

    $date = $request->get("dateChoice");
    $author = $request->get("authorChoice");

    return $this->render("pages/home.html.twig", [
      "allAuthors" => $allAuthors,
      "articleDateMax" => $date,
      "articleOnlyAuthor" => $author
    ]);
  }

  /**
   * @Route("/article/{articleId}", name="articlePage", methods={"GET"})
   */
  public function articlePage(string $articleId = null)
  {
    $article = $this->articleRepository->find($articleId);

    return $this->render("pages/article.html.twig", [
      "article" => $article,
      "articleId" => $articleId
    ]);
  }

  /**
   * @Route("/articleList/", name="articleList", methods={"GET", "POST"})
   */
  public function listeArticle(Request $request)
  {
    $dateMax = $request->get("dateMax");
    $authorOnly = $request->get("authorOnly");

    // Ces 6 lignes permettent de rendre null la variable $dateMax si elle ne correspond pas au format attendu
    if (!is_null($dateMax)) {
      $testDateMaxFormat = preg_match('/\d{4}-\d{2}-\d{2}/', $dateMax); // Vérification du format souhaité en regex
      if ($testDateMaxFormat != 1) {
        $dateMax = null;
      }
    }

    // Si un auteur n'a pas été choisi alors on passe la variable de "" a null
    if (!is_null($authorOnly)) {
      if ($authorOnly == "") {
        $authorOnly = null;
      }
    }

    $articles = [];

    if (is_null($dateMax) && is_null($authorOnly)) {
      $articles = $this->articleRepository->findAll();
    } else if (!is_null($dateMax) && is_null($authorOnly)) {
      $articles = $this->articleRepository->findByMaximalDate($dateMax);
    } else if (is_null($dateMax) && !is_null($authorOnly)) {
      $articles = $this->articleRepository->findByAuthor($authorOnly);
    } else if (!is_null($dateMax) && !is_null($authorOnly)) {
      $articlesDateFiltered = $this->articleRepository->findByMaximalDate($dateMax);
      $articlesAuthorFiltered = $this->articleRepository->findByAuthor($authorOnly);
      foreach ($articlesDateFiltered as $articleDateFiltered) {
        foreach ($articlesAuthorFiltered as $articleAuthorFiltered) {
          if ($articleDateFiltered->getId() == $articleAuthorFiltered->getId()) {
            array_push($articles, $articleAuthorFiltered);
          }
        }
      }
    }

    return $this->render("partials/listeArticles.html.twig", [
      'articles' => $articles
    ]);
  }

  /**
   * @Route("articleform/", name="articleForm", methods={"GET", "POST"})
   */
  public function editArticle(Request $request)
  {
    $articleId = $request->get("articleId");

    if (!is_null($articleId)) {
      $article = $this->articleRepository->find($articleId);
    } else {
      $article = new Article();
    }

    $form = $this->createForm(CreateArticleFormType::class, $article); // On crée l'objet formulaire

    $form->handleRequest($request); // On gère la requête du formulaire

    if ($form->isSubmitted() && $form->isValid()) { // Si le formulaire est envoyé et valide
      $articleManager = $this->getDoctrine()->getManager(); // On définit le manager doctrine
      $articleManager->persist($form->getData()); // On stocke en mémoire l'objet récupéré
      $articleManager->flush(); // On enregistre tous les objets stockés en mémoire
      return $this->redirectToRoute("accueilPage");
    }

    return $this->render("pages/editArticle.html.twig", [
      "article" => $article,
      "form" => $form->createView()
    ]);
  }

  /**
   * @Route("/delete", name="articleDel")
   */
  public function deleteArticle(Request $request)
  {
    $article = $this->articleRepository->find($request->get("articleId"));

    if ($this->isCsrfTokenValid("delete" . $article->getId(), $request->get("_token"))) {
      $articleManager = $this->getDoctrine()->getManager();
      $articleManager->remove($article);
      $articleManager->flush();

      $this->addFlash("success", "l'article a bien été supprimé.");

      return $this->redirectToRoute("accueilPage");
    }
  }
}
