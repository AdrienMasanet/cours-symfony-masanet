CREATION D'UN PROJET SYMFONY

symfony new my_project_name


INSTALLER DES PACKAGES

composer req monpackage


CREATION DE MODELES ET DE PROPRIETES

php bin/console make:entity

Si on veut rajouter une ou des propriétés à une entité déjà existante, on doit faire la même commande avec le nom de l'entité voulue juste après make:entity


MODIFIER LES PROPRIETES D'UN OBJET EN ENREGISTRANT DANS LA BDD

On peut créer l'objet en mode $truc = new Truc();
On peut modifier ses propriétés avec les méthodes du model de l'objet par exemple $truc->setAdresse("adresse")->setName("nom")
Si on utilise des setters, c'est qu'on veut sauvegarder ces modifications dans la bdd
Du coup pour ça, il faut faire un persist avec le doctrineManager->persist(), c'est un peu comme un query prepare (en gros)
Le doctrineManager->flush() exécute toutes les commandes doctrineManager->persist() en attente d'être flushées, c'est un peu comme un query execute (en gros)

Le RepositoryManager d'une classe est accédé dans les controllers, essentiellement pour récupérer des tableaux d'objets/faire du select dans la bdd (sauf exception selon la situation), les modifications des propriétés des objets sont généralement faites avec le doctrineManager


CREATION ET ALIMENTATION DES TABLES

php bin/console make:migration // Cette commande Créé les fichiers php de migration (fichiers contenant les queries de creation des tables qui peuvent être exécutés avec la commande d:m:m)

php bin/console doctrine:migrations:migrate // Cette commande rentre les tables dans la base de données

php bin/console doctrine:fixtures:load // Cette commande permet de remplir la base de données avec les fixtures paramétrées dans le fichier AppFixtures.php


TWIG

Pour la concaténation dans twig, utiliser un ~

Pour insérer variable : {{maVariable}}
Pour accéder à la propriété d'un objet : {{monObjet.propriete}}

Si un bloc est déjà déclaré avant et qu'on le réécrit autre part, il gardera la position où il a été déclaré la première fois (par exemple si on définit un block javascript dans le head, il y restera même si on déclare un nouveau block du même nom)

Pour redéfinir un bloc sans en écraser le contenu (sa position initiale ne change pas dans tous les cas) il faut utiliser "extends" au lieu de "block"


AUTRE

Quand on utilise lando ou qu'on publie sur un serveur apache, il ne faut pas oublier le composer req symfony/apache-pack
Pour que le routing et tout ça fonctionne comme il faut